package game_cat;

import java.awt.Graphics;

public abstract class Overlay {
	public abstract void render(Graphics g);
}
