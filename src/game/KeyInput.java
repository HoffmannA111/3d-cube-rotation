package game;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class KeyInput extends KeyAdapter {

	private Handler handler;

	public KeyInput(Handler handler) {
		this.handler = handler;
	}

	public void keyPressed(KeyEvent e) {
		int key = e.getKeyCode();

		if(key == KeyEvent.VK_W){
			handler.camera.forward = true;
		}
		if(key == KeyEvent.VK_S){
			handler.camera.backward = true;
		}
		if(key == KeyEvent.VK_D){
			handler.camera.right = true;
		}
		if(key == KeyEvent.VK_A){
			handler.camera.left = true;
		}
		if(key == KeyEvent.VK_SPACE){
			handler.camera.up = true;
		}
		if(key == KeyEvent.VK_CONTROL){
			handler.camera.down = true;
		}
		if(key == KeyEvent.VK_SHIFT){
			handler.camera.shift = true;
		}
	}

	public void keyReleased(KeyEvent e) {
		int key = e.getKeyCode();

		if(key == KeyEvent.VK_W) handler.camera.forward = false;
		if(key == KeyEvent.VK_S) handler.camera.backward = false;
		if(key == KeyEvent.VK_D) handler.camera.right = false;
		if(key == KeyEvent.VK_A) handler.camera.left = false;
		if(key == KeyEvent.VK_SPACE) handler.camera.up = false;
		if(key == KeyEvent.VK_CONTROL) handler.camera.down = false;
		if(key == KeyEvent.VK_SHIFT) handler.camera.shift = false;
	}

}
